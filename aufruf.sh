#! /usr/bin/env bash

S=tls-ciphersuite-support.py

./$S    TLS_ECDHE_RSA_WITH_AES_256_GCM_SHA384 \
        TLS_ECDHE_RSA_WITH_AES_128_GCM_SHA256 \
        TLS_ECDHE_RSA_WITH_AES_256_CBC_SHA \
        TLS_ECDHE_RSA_WITH_AES_128_CBC_SHA \
        TLS_DHE_RSA_WITH_AES_256_CBC_SHA \
        TLS_DHE_RSA_WITH_AES_128_CBC_SHA >vorher.txt

./$S    TLS_ECDHE_RSA_WITH_AES_256_GCM_SHA384 \
        TLS_ECDHE_RSA_WITH_AES_128_GCM_SHA256 \
        TLS_ECDHE_RSA_WITH_AES_256_CBC_SHA \
        TLS_ECDHE_RSA_WITH_AES_128_CBC_SHA >nachher.txt

diff -y {vorher,nachher}.txt


