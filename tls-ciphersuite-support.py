#! /usr/bin/env python3

import sys
import json

if len(sys.argv)==1 or not sys.argv[1].startswith('TLS'):
    print("Syntax:\n", sys.argv[0], "TLS_Ciphersuite_1 ... TLS_Ciphersuite_n")
    sys.exit(1)

Suchziele={ }
for i in range(1,len(sys.argv)):
    if sys.argv[i].startswith("TLS_"):
        Suchziele[sys.argv[i]]=1
    else:
        print('Fehler bei "{}", Ciphersuite-name beginnt immer mit TLS_'.format(sys.argv[1]))
        sys.exit(1)

try:
    f=open('getClients.json', "rt")
    d = json.load(f)
    f.close()
except:
    print("Fehler beim Öffnen von getClient.jon")
    sys.exit(1)

Enthalten=[]; Fehlt=[]
for i in d:
    Gefunden=False
    for ciphersuite in i['suiteNames']:
        if ciphersuite in Suchziele:
            Gefunden=True
            break
    
    Name=i['name']+' '+i['version']
    Enthalten.append(Name) if Gefunden else Fehlt.append(Name)


print("========== Ciperhsuiten: {}".format(
        ' '.join(
                sorted(Suchziele.keys())
            )
        )
      )
print("========== werden unterstützt von:")
for i in sorted(Enthalten):
    print(i)

print("========== werden nicht unterstützt von:")
for i in sorted(Fehlt):
    print(i)

